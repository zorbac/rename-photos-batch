# -*-coding:utf8;-*-
"""
@author: jingl3s

"""

# license
#
# Producer 2017 jingl3s
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import time
import os
import sys
import subprocess
import glob

"""
lanceur qi appel le point d'enrre en liatant tpus les fichiers present dans le dossier de travail'
"""


if __name__ == "__main__":

    # Recuperation liste de fichiers
    dossier_src = os.getcwd()
    dossier = os.path.dirname(__file__)
    dossier = os.path.realpath(dossier)
    os.chdir(dossier)

    #    contenu_dossier =os.listdir('fichiers_example2')
    #   print(contenu_dossier)

    # Utilisation se glob pour avoir le chemin complet
    contenu_dossier = glob.glob("fichiers_example2/*")

    # Commande python pour utiliser le meme
    print(sys.version)
    print(sys.executable)
    python_cmd = sys.executable

    list_cmd_lot1 = list()
    list_cmd = list()
    list_cmd.append(python_cmd)
    list_cmd.append(os.path.join(dossier, "main_parametre.py"))
    list_cmd.extend(contenu_dossier)

    list_cmd_lot1.append(list_cmd)

    # Preparation surveillance programmwe si plusieurs executions
    list_popen = list()

    for cmd in list_cmd_lot1:
        popen = subprocess.Popen(cmd)
        list_popen.append(popen)

    boucle_processus = True
    while boucle_processus:
        # Detection si un processus est terminé pour arreter tout le monde
        for index, popen in enumerate(list_popen):
            if popen.poll() is not None:
                del list_popen[index]
                boucle_processus = False
        time.sleep(1)

    # Destruction de tous les processus
    if not boucle_processus:
        for popen in list_popen:
            popen.kill()
