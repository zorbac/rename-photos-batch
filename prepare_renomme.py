import re
import os
import logging


class Fichier:
    """
    Classe pour memoriser les informations du renommage d'un fichier'
    """

    __slice__ = ["dossier", "source", "dest_prefix", "dest_fin"]

    def __init__(self, dossier, source, dest_prefix, dest_fin):
        self.dossier = dossier
        self.source = source
        self.dest_prefix = dest_prefix
        self.dest_fin = dest_fin
        self._milieu = ""

    def get_source(self):
        return os.path.join(self.dossier, self.source)

    def get_dest(self, p_insere: str = ""):
        return os.path.join(self.dossier, self._get_dest_filename(p_insere))

    def _get_dest_filename(self, p_insere: str):
        return self.dest_prefix + self._milieu + p_insere + self.dest_fin

    def changement(self):
        return self.source != self.dest_prefix + self.dest_fin

    def insert_milieu(self, p_s_inserer_milieu):
        self._milieu = p_s_inserer_milieu

    def __str__(self):
        return "({}), {} -> {}".format(
            self.dossier, self.source, self._get_dest_filename("")
        )

    def __repr__(self):
        return self.__str__()

    def __eq__(self, p_fichier):
        return self.get_dest() == p_fichier.get_dest()
        # return self.dossier == p_fichier.dossier and self.dest_prefix == p_fichier.dest_prefix and self.dest_fin == p_fichier.dest_fin


class Renomme(object):
    """
    Preparation et renommage de fichiers
    """

    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.debug("---------------")
        self._list_cle_suppr = list()
        self._pattern = re.compile("([\d\._]+)(.*)(\.\w+)$")
        self._separateur = "_"

    def set_separateur(self, p_s_separateur):
        self._separateur = p_s_separateur

    def set_cle_debut_supprime(self, p_list_cle_supprime):
        self._list_cle_suppr = p_list_cle_supprime
        self._logger.debug(self._list_cle_suppr)

    def capture_nom_fichier(self, nom_fichier):
        """
        Capture les noms de fichiers avec le format attendu
        """
        match = self._pattern.match(nom_fichier)
        if match:
            nom_sortie = match.groups()
            if not isinstance(nom_sortie, tuple):
                nom_sortie = nom_fichier
        else:
            nom_sortie = (nom_fichier,)
        return nom_sortie

    def prepare_list(self, p_list_fichier, p_list_texte):
        """
        pour chaque fichier creation du nom de sortie
        """
        p_s_texte = self._separateur.join(p_list_texte)
        self._list_fich = list()
        for fichier in p_list_fichier:
            self._logger.debug(fichier)
            fichier2 = os.path.basename(fichier)
            fichier_dossier = os.path.dirname(fichier)
            fichier_coupe = self.capture_nom_fichier(fichier2)
            if 1 == len(fichier_coupe):
                # Aucun changement car pas de dcoupage possible
                fichier_sortie_debut = fichier
                fichier_sortie_fin = ""

            else:
                self._logger.debug("coupe : {}".format(fichier_coupe))
                debut_sep = ""
                if not fichier_coupe[0].lower().endswith(self._separateur):
                    debut_sep = self._separateur

                if self._detect_cle_suppr(fichier_coupe[1].lower()):
                    fichier_sortie_debut = fichier_coupe[0] + debut_sep

                    fichier_sortie_fin = p_s_texte + fichier_coupe[2]

                else:
                    # Recherche si des texte sont deja present
                    list_text = list()
                    for nouveau_texte in p_list_texte:
                        if str(nouveau_texte) not in str(fichier_coupe[1]):
                            list_text.append(nouveau_texte)
                    if len(list_text) > 0:
                        s_texte = "_" + self._separateur.join(list_text)
                    else:
                        s_texte = ""

                    fichier_sortie_debut = fichier_coupe[0] + debut_sep

                    if fichier_coupe[1] != "":
                        fichier_sortie_fin = (
                            fichier_coupe[1] + s_texte + fichier_coupe[2]
                        )
                    else:
                        fichier_sortie_fin = s_texte[1:] + fichier_coupe[2]

            #             self._add_new_file(fichier_dossier, fichier2,
            #                                fichier_sortie_debut, fichier_sortie_fin)

            fich = Fichier(
                fichier_dossier, fichier2, fichier_sortie_debut, fichier_sortie_fin
            )

            self._logger.debug(fich)
            self._list_fich.append(fich)

        # Recherche de doublons
        self._modify_for_duplication()

        # TODO: Ajouter un traitement complet en attendant une correction
        # Analyse si fichier de sortie ayant déja le meme nom
        list_tous_les_fichiers = [fichier.get_dest() for fichier in self._list_fich]
        set_tous_les_fichiers = set(list_tous_les_fichiers)

        if len(set_tous_les_fichiers) != len(list_tous_les_fichiers):
            self._list_fich = list()
            list_identicals = list_tous_les_fichiers[:]
            for x in set_tous_les_fichiers:
                list_identicals.remove(x)
            self._logger.error(
                "Fichiers de sortie observé en double : {}".format(list_identicals)
            )

        return self._list_fich

    def _detect_cle_suppr(self, p_s_chaine):
        for mot_cle in self._list_cle_suppr:
            if p_s_chaine.startswith(mot_cle):
                return True
        return False

    def renomme(self):
        compteur = 0
        for fich in self._list_fich:
            self._logger.debug("Fichier: {}".format(fich))
            if not fich.changement():
                continue
            if os.path.exists(fich.get_source()):
                fich_sortie = fich.get_dest()
                while os.path.exists(fich_sortie):
                    fich_sortie = fich.get_dest(
                        str(compteur).zfill(2) + self._separateur
                    )
                    compteur += 1
                os.rename(fich.get_source(), fich_sortie)
            else:
                self._logger.warning("fichier ignore")
            pass

    def _modify_for_duplication(self):
        last_fichier = None
        for fichier_courant in self._list_fich:
            if last_fichier is not None and last_fichier == fichier_courant:
                last_fichier.insert_milieu(
                    str(self._counter).zfill(2) + self._separateur
                )
                self._counter += 1
                fichier_courant.insert_milieu(
                    str(self._counter).zfill(2) + self._separateur
                )
            else:
                self._counter = 0
                last_fichier = fichier_courant


if __name__ in "__main__":
    logging.basicConfig()
    logger = logging.getLogger("")
    logger.setLevel(logging.DEBUG)
    ren = Renomme()
    ren.set_cle_debut_supprime(["dsc", "mah0"])
    list_fichier = list()
    entree = "patate/2017.03.12_45_dsc7848.jpg"
    list_fichier.append(entree)
    entree = "patate/2017.03.12_45_dsc7849.jpg"
    list_fichier.append(entree)
    entree = "2017.03.12_45_mat.JPG"
    list_fichier.append(entree)
    entree = "2017.03.12_4545.JPG"
    list_fichier.append(entree)

    entree = "2017.03.12_dsc7848.mpeg"
    list_fichier.append(entree)

    entree = "2017.03.12_dsc7848.mpeg"
    list_fichier.append(entree)

    entree = "gggg.jpg"
    list_fichier.append(entree)

    entree = "10"
    list_fichier.append(entree)
    sort = ren.prepare_list(list_fichier, ["coucou", "mat"])
    print(sort)

    fich = Fichier(".", "abc.jpg", "bc", "_ttt.jpg")
    print(fich.get_source())
    print(fich.get_dest())
    print(fich)

    list_fichier = list()
    entree = "patate/2017.03.12_45_dsc7848.jpg"
    list_fichier.append(entree)
    entree = "patate/2017.03.12_45_dsc7849.jpg"
    list_fichier.append(entree)
    sort = ren.prepare_list(list_fichier, ["coucou", "mat"])
    print(sort)
