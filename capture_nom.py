import re


def capture_nom_fichier(nom_fichier):

    pattern = re.compile("([\d\._]+)(.*)(\.\w+)$")
    match = pattern.match(nom_fichier)
    if match:
        nom_sortie = match.groups()
    else:
        nom_sortie = nom_fichier
    return nom_sortie
